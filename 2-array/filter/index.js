function chooseMultiplesOfThree(collection) {
  // TODO 1: 在这里写实现代码
  return collection.filter(item => item % 3 === 0);
}

function chooseNoRepeatNumber(collection) {
  // TODO 2: 在这里写实现代码
  const result = [];
  result.push(collection[0]);
  for (let i = 1; i < collection.length; i += 1) {
    if (collection[i] !== collection[i - 1]) result.push(collection[i]);
  }
  return result;
}

export { chooseMultiplesOfThree, chooseNoRepeatNumber };
